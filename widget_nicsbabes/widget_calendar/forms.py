from django import forms
from .models import Event

class EventCreateForm(forms.ModelForm):
    class Meta:
        model = Event
        fields = ['activity', 'target_datetime', 'estimated_hours',
         'location', 'course']
        widgets = {
            'target_datetime': forms.DateTimeInput(attrs={'type': 'datetime-local'})
        }
