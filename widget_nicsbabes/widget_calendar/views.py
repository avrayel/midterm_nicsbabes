from django.shortcuts import render
from django.http import HttpResponse
from .models import Event, Location
from .forms import EventCreateForm
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.detail import DetailView

def index(HttpRequest):
    events = Event.objects.all()
    args = { 'events':events }
    return render(HttpRequest, 'widget_calendar/calendar.html', args)

class EventDetailView(DetailView):
    model = Event
    template_name = "widget_calendar/event-details.html"

class EventCreateView(CreateView):
    form_class = EventCreateForm
    # taken from: https://stackoverflow.com/questions/27321692/override-a-django-generic-class-based-view-widget
    template_name = "widget_calendar/event-add.html"

class EventUpdateView(UpdateView):
    model = Event
    form_class = EventCreateForm
    template_name="widget_calendar/event-edit.html"
