from django.contrib import admin
from .models import Location, Event


class LocationAdmin(admin.ModelAdmin):
    model = Location
    list_display = ("venue", "mode")
    search_fields = ("venue",)
    list_filter = ("mode",)


class EventAdmin(admin.ModelAdmin):
    model = Event
    list_display = ("activity", "target_datetime", "course")
    search_fields = ("activity", "course")
    list_filter = ("course",)
    

admin.site.register(Location, LocationAdmin)
admin.site.register(Event, EventAdmin)
