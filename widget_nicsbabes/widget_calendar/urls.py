from django.urls import path
from .views import index, EventDetailView, EventCreateView, EventUpdateView


urlpatterns = [
    path('', index, name='index'),
    path('events/<int:pk>/details', EventDetailView.as_view(), name='event_details'),
    path('events/<int:pk>/edit', EventUpdateView.as_view(), name='event_edit'),
    path('events/add', EventCreateView.as_view(), name='event_add'),

]

app_name = "widget_calendar"