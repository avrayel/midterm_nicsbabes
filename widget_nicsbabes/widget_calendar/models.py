from django.db import models
from assignments.models import Course
from django.urls import reverse

class Location(models.Model):
    ONSITE = 'onsite'
    ONLINE = 'online'
    HYBRID = 'hybrid'
    MODE_CHOICES = [
        (ONSITE, 'Onsite'),
        (ONLINE, 'Online'),
        (HYBRID, 'Hybrid'),
    ]

    mode = models.CharField(
        max_length = 6,
        choices = MODE_CHOICES,
        default = ONSITE,
    )
    venue = models.CharField(max_length=255)

    def __str__(self):
        return '{} - {}'.format(
            self.venue, 
            self.mode
        )


class Event(models.Model):
    target_datetime = models.DateTimeField()
    activity = models.TextField()
    estimated_hours = models.FloatField()
    location = models.ForeignKey(Location, on_delete = models.CASCADE)
    course = models.ForeignKey(Course, on_delete = models.CASCADE)

    def __str__(self):
        return '{} by {}'.format(
            self.activity, 
            self.course
        )
    def get_absolute_url(self):
        return reverse("widget_calendar:event_details", kwargs={"pk": self.pk})
    
    def get_date(self): 
        return self.target_datetime.strftime("%m/%d/%Y")

    def get_time(self):
        return self.target_datetime.strftime("%I:%M %p")