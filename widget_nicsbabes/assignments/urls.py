from django.urls import path
from .views import *

urlpatterns = [
	path('', index, name='index'),
	path('<int:pk>/details', AssignmentsDetailView.as_view(), name='assignment-details'),
	path('<int:pk>/edit', AssignmentsUpdateView.as_view(), name='assignment-edit'),
	path('add', AssignmentsCreateView.as_view(), name='assignment-add'),

]
# This might be needed, depending on your Django version
app_name = "assignments"
