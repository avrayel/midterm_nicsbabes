from django.db import models
from django.urls import reverse

class Course(models.Model):

    code = models.CharField(max_length=10)
    title = models.CharField(max_length=255)
    section = models.CharField(max_length=3)

    def __str__(self):
        return '{} - {}'.format(self.code, self.section)

class Assignment(models.Model):

    name = models.CharField(max_length=255)
    description = models.TextField()
    course = models.ForeignKey(Course,on_delete=models.CASCADE,default="")
    perfect_score = models.IntegerField()

    def __str__(self):
        return '{}'.format(self.name)

    def passing_score(self):
        return (self.perfect_score * 60)//100

    def get_absolute_url(self):
        return reverse('assignments:assignment-details', kwargs={'pk':self.pk})
