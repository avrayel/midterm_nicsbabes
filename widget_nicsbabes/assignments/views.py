from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import ListView, DetailView, CreateView, UpdateView
from .models import Assignment, Course

def index(request):
    assignments = Assignment.objects.all()
    args = {'assignments':assignments}
    return render(request, 'assignments/assignments.html', args)

class AssignmentsView(ListView):
    model = Assignment
    template_name = 'assignments/assignments.html'

class AssignmentsDetailView(DetailView):
    model = Assignment
    template_name = 'assignments/assignment-details.html'

class AssignmentsUpdateView(UpdateView):
    model = Assignment
    fields = "__all__"
    template_name = 'assignments/assignment-edit.html'

class AssignmentsCreateView(CreateView):
    model = Assignment
    fields = "__all__"
    template_name = 'assignments/assignment-add.html'
