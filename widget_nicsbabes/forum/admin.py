from django.contrib import admin

from .models import ForumPost, Reply


class ForumPostAdmin(admin.ModelAdmin):
	model = ForumPost
	list_display = ("title","body","author","pub_datetime")
	search_fields = ("title","author")
	list_filter = ("title","author")

class ReplyAdmin(admin.ModelAdmin):
	model = Reply
	list_display = ("body","author","pub_datetime","forum_post")
	search_fields = ("author",)
	list_filter = ("author",)


admin.site.register(ForumPost, ForumPostAdmin)
admin.site.register(Reply, ReplyAdmin)