from django.db import models
from dashboard.models import WidgetUser
from django.urls import reverse
from django.utils import timezone

class ForumPost(models.Model):
	title = models.CharField(max_length=255,default="")
	body = models.TextField(default="")
	author = models.ForeignKey(WidgetUser,on_delete=models.CASCADE)
	pub_datetime = models.DateTimeField(default=timezone.now)

	def __str__(self):
		return '{}'.format(self.title)

	def get_absolute_url(self):
		return reverse("forum:forumpost-details",kwargs={'pk':self.pk})

class Reply(models.Model):
	body = models.TextField(default="")
	author = models.ForeignKey(WidgetUser,on_delete=models.CASCADE)
	pub_datetime = models.DateTimeField()
	forum_post = models.ForeignKey(ForumPost,on_delete=models.CASCADE,default="",related_name='forum_reply')

	def __str__(self):
		return '{}'.format(self.body)