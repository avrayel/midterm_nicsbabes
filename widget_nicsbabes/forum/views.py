from django.shortcuts import render
from django.http import HttpResponse
from .models import ForumPost, Reply
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView

def forum(request):
    return render(request, 'forum/forum.html', {'forum':ForumPost.objects.order_by('-pub_datetime')
})

class ForumPostDetailView(DetailView):
    model = ForumPost
    template_name = "forum/forumpost-details.html"

class ForumPostCreateView(CreateView):
    model = ForumPost
    fields = ['title','body','author']
    template_name = 'forum/forumpost-add.html'

class ForumPostUpdateView(UpdateView):
    model = ForumPost
    fields = ['title','body','author']
    template_name = 'forum/forumpost-edit.html'