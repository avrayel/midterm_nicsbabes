from .models import Announcement
from django.shortcuts import render
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView

def announcements(request):
    return render (request, 'announcement_board/announcements.html', 
        {'announcements':Announcement.objects.order_by('-pub_datetime')})

class AnnouncementDetailView(DetailView):
    model = Announcement
    template_name = "announcement_board/announcement-details.html"


class AnnouncementCreateView(CreateView):
    model = Announcement
    template_name = "announcement_board/announcement-add.html"
    fields = ['title','body','author']

class AnnouncementUpdateView(UpdateView):
    model = Announcement
    template_name = "announcement_board/announcement-edit.html"
    fields = ['title','body','author']