from django.urls import path
from .views import (
    announcements, AnnouncementCreateView, AnnouncementDetailView, AnnouncementUpdateView
) 

urlpatterns = [
    path('',announcements, name='announcements'),
    path('<int:pk>/details/',AnnouncementDetailView.as_view(),name='announcement-details'),
    path('add/',AnnouncementCreateView.as_view(),name='announcement-add'),
    path('<int:pk>/edit/',AnnouncementUpdateView.as_view(),name='announcement-edit'),
]

app_name = 'announcement_board'
