from django.db import models
from dashboard.models import WidgetUser
from django.urls import reverse
from django.utils import timezone

class Announcement(models.Model):
    title = models.CharField(max_length=100,default="")
    body = models.TextField(default="")
    author = models.ForeignKey(WidgetUser,on_delete=models.CASCADE)
    pub_datetime = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return '{} by {}'.format(self.title, self.author)
    
    def get_absolute_url(self):
        return reverse("announcement_board:announcement-details",kwargs={'pk':self.pk})
    
    def get_reactions(self):
        reacts = {'Like':0,'Love':0,'Angry':0}
        for key in reacts.keys():
            try:
                reacts[key]=Reaction.objects.get(announcement=self,name=key).tally
            except:
                reacts[key]=0
        return reacts

class Reaction(models.Model):
    name = models.CharField(max_length=20,default="")

    reactions = [('Like','Like'),
                 ("Love", 'Love'),
                 ('Angry', 'Angry')
    ]
    name = models.CharField(choices=reactions,max_length=6)
    tally = models.IntegerField(default=0)
    announcement = models.ForeignKey(Announcement,on_delete=models.CASCADE)
    
    def __str__(self):
        return '{} reaction in "{}"'.format(self.name, self.announcement.title)
    
