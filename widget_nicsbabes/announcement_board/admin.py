from django.contrib import admin
from .models import Announcement, Reaction

class AnnouncementAdmin(admin.ModelAdmin):
    model = Announcement
    list_dsiplay = ("title","body","author","pub_datetime")
    search_fields = ("title", "author")
    list_filter = ("title","author")

class ReactionAdmin(admin.ModelAdmin):
    model = Reaction
    list_dsiplay = ("name","tally","announcement")
    search_fields = ("name","announcement")
    list_filter = ("name","announcement")


admin.site.register(Announcement,AnnouncementAdmin)
admin.site.register(Reaction,ReactionAdmin)
