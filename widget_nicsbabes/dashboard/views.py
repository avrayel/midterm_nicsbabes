from django.shortcuts import render
from .models import WidgetUser
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView


def index(request):
    user = WidgetUser.objects.all()
    args = {'user':user}
    return render(request, 'dashboard/dashboard.html', args)

class WidgetUserListView(ListView):
    template_name='dashboard/dashboard.html'
    
class WidgetUserDetailView(DetailView):
    model = WidgetUser
    template_name='dashboard/widgetuser-details.html'

class WidgetUserCreateView(CreateView):
    model = WidgetUser
    template_name = 'dashboard/widgetuser-add.html'
    fields = '__all__'

class WidgetUserUpdateView(UpdateView):
    model = WidgetUser
    template_name = 'dashboard/widgetuser-edit.html'
    fields = '__all__'