from django.db import models
from django.urls import reverse

class Department(models.Model):
    dept_name = models.CharField(max_length=100,unique=True,default="")
    home_unit = models.CharField(max_length=100,unique=True,default="")

    def __str__(self):
        return '{}'.format(self.dept_name)

class WidgetUser(models.Model):
    first_name = models.CharField(max_length=100,default="")
    middle_name = models.CharField(max_length=100,default="")
    last_name = models.CharField(max_length=100,default="")
    department = models.ForeignKey(Department,on_delete=models.CASCADE)

    def __str__(self):
        return '{}, {}'.format(self.last_name, self.first_name)
    
    def get_absolute_url(self):
        return reverse('dashboard:user_details', kwargs={'pk': self.pk})
    