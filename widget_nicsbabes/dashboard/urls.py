from django.urls import path
from .views import *

urlpatterns = [
    path('',index,name='index'),
    path('dashboard/',WidgetUserListView.as_view(),name='user_list'),
    path('widgetusers/<int:pk>/details/',WidgetUserDetailView.as_view(),name='user_details'),
    path('widgetusers/add/',WidgetUserCreateView.as_view(),name='new_user'),
    path('widgetusers/<int:pk>/edit/',WidgetUserUpdateView.as_view(),name='edit_user'),
]

app_name = 'announcement_board'